package de.pro_crafting.region;

import org.bukkit.World;

import de.pro_crafting.common.Point;

public class ImmutableRegion extends Region {

	private Point min;
	private Point max;

	public ImmutableRegion(String id, World world, Point min, Point max) {
		super(id, world);
		this.min = min;
		this.max = max;
	}

	@Override
	public Point getMin() {
		return this.min;
	}

	@Override
	public Point getMax() {
		return this.max;
	}
}
