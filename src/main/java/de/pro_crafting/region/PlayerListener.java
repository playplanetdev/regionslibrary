package de.pro_crafting.region;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import de.pro_crafting.region.events.PlayerRegionChangeEvent;

public class PlayerListener implements Listener {
	RegionManager rMan;

	public PlayerListener(RegionManager rMan)
	{
		this.rMan = rMan;
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void playerMoveHandler(PlayerMoveEvent event) {
		if (event.getTo().getBlock().equals(event.getTo().getBlock())) {
			return;
		}
		event.setCancelled(this.fireRegionChangeEvent(event.getPlayer(),
				event.getFrom(), event.getTo(), event.isCancelled()));
	}

	@EventHandler(priority = EventPriority.LOW)
	public void playerTeleportHandler(PlayerTeleportEvent event) {
		event.setCancelled(this.fireRegionChangeEvent(event.getPlayer(),
				event.getFrom(), event.getTo(), event.isCancelled()));
	}

	private boolean fireRegionChangeEvent(Player player, Location from,
			Location to, boolean isCancelled) {
		List<Region> leaves = rMan.getRegions(from);
		List<Region> enters = rMan.getRegions(to);
		leaves.removeAll(enters);
		enters.removeAll(leaves);

		if (leaves.size() == enters.size()) {
			return isCancelled;
		}
		boolean isEntering = leaves.size() < enters.size();

		PlayerRegionChangeEvent regionChangeEvent = new PlayerRegionChangeEvent(
				player, leaves, enters, isEntering);
		regionChangeEvent.setCancelled(isCancelled);
		Bukkit.getPluginManager().callEvent(regionChangeEvent);

		return regionChangeEvent.isCancelled();
	}
}
