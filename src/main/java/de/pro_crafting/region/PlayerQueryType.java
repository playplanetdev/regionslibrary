package de.pro_crafting.region;

public enum PlayerQueryType {
	Owner,
	Member,
	Group
}
