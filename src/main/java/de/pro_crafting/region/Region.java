package de.pro_crafting.region;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import de.pro_crafting.common.Point;
import de.pro_crafting.region.domain.Domain;
import de.pro_crafting.region.flags.Flag;
import de.pro_crafting.region.flags.StateValue;
import de.pro_crafting.region.provider.Provider;

public class Region {
	private UUID world;
	private String id;
	private Provider provider;
	private Domain owners;
	private Domain members;
	
	public Region(String id, World world, Provider provider) {
		this.id = id;
		this.provider = provider;
		this.world = world.getUID();
	}

	Region(String id, World world) {
		this.id = id;
		this.world = world.getUID();
	}
 
	public World getWorld() {
		return Bukkit.getWorld(world);
	}

	public String getId() {
		return this.id;
	}

	public Point getMin() {
		return provider.getMinimumPoint(this);
	}

	public Point getMax() {
		return provider.getMaximumPoint(this);
	}
	
	public boolean hasMembersOrOwners() {
		return provider.getPlayers(this, PlayerQueryType.Member).size() > 0 ||
				provider.getPlayers(this, PlayerQueryType.Owner).size() > 0;
	}
	
	public Domain getOwners() {
		if (owners == null)
		{
			owners = new Domain(this, PlayerQueryType.Owner, this.provider);
		}
		return owners;
	}

	public Domain getMembers() {
		if (members == null)
		{
			members = new Domain(this, PlayerQueryType.Member, this.provider);
		}
		return members;
	}

	public void remove() {
		this.provider.removeRegion(this);
	}

	public void setFlag(Flag flag, StateValue to) {
		this.provider.setFlag(this, flag, to);
	}

	public boolean contains(Location location) {
		if (this.getWorld().equals(location.getWorld()))
		{
			return false;
		}
		if (this.getMin().getX() < location.getX() && this.getMin().getY() < location.getY() && this.getMin().getZ() < location.getZ())
		{
			return false;
		}
		if (this.getMax().getX() > location.getX() && this.getMax().getY() > location.getY() && this.getMax().getZ() > location.getZ())
		{
			return false;
		}
		return true;
	}
}
