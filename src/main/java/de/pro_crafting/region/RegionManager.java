package de.pro_crafting.region;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.event.Listener;

import de.pro_crafting.region.provider.Provider;
import de.pro_crafting.region.provider.plugins.WorldGuard;

public class RegionManager implements Listener {
	private Map<ProtectionPlugin, Provider> providers;

	RegionManager() {
		loadRegionPlugins();
	}

	private void loadRegionPlugins() {
		providers = new HashMap<ProtectionPlugin, Provider>();
		hook(ProtectionPlugin.WorldGuard, "WorldGuard", WorldGuard.class);
	}

	private void hook(ProtectionPlugin plugin, String name,
			Class<? extends Provider> hookClass) {
		if (Bukkit.getPluginManager().getPlugin(name) != null) {
			try {
				this.providers.put(plugin, hookClass.getConstructor()
						.newInstance());
				Bukkit.getLogger().info("Hooked into: "+plugin.toString());
			} catch (Exception ex) {

			}
		}
	}

	public void save() {
		for (Provider provider : providers.values()) {
			provider.save();
		}
	}

	public boolean create(ImmutableRegion rg, ProtectionPlugin plugin) {
		return this.providers.get(plugin).createRegion(rg);
	}

	public List<Region> getRegions(Location at) {
		List<Region> ret = new ArrayList<Region>();
		for (Provider provider : providers.values()) {
			ret.addAll(provider.getRegions(at));
		}
		return ret;
	}

	public List<Region> getRegions(OfflinePlayer player, World where) {
		List<Region> ret = new ArrayList<Region>();
		for (Provider provider : providers.values()) {
			ret.addAll(provider.getRegions(player, where));
		}
		return ret;
	}

	public List<Region> getRegions(String id, World where) {
		List<Region> ret = new ArrayList<Region>();
		for (Provider provider : providers.values()) {
			ret.add(provider.getRegion(id, where));
		}
		return ret;
	}

	public Map<ProtectionPlugin, List<Region>> getRegions(World where) {
		Map<ProtectionPlugin, List<Region>> ret = new HashMap<ProtectionPlugin, List<Region>>();
		for (ProtectionPlugin plugin : providers.keySet()) {
			ret.put(plugin, providers.get(plugin).getRegions(where));
		}
		return ret;
	}
}
