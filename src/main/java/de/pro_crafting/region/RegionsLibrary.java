package de.pro_crafting.region;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class RegionsLibrary extends JavaPlugin {
	private RegionManager rgm;
	private PlayerListener playerListener;
	
	@Override
	public void onEnable() {
		this.rgm = new RegionManager();
		this.playerListener = new PlayerListener(rgm);
		Bukkit.getPluginManager().registerEvents(this.playerListener, this);
	}

	@Override
	public void onDisable() {
		HandlerList.unregisterAll(this.playerListener);
		rgm.save();
	}

	public RegionManager getRegionManager() {
		return this.rgm;
	}
}
