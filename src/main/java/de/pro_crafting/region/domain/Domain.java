package de.pro_crafting.region.domain;

import java.util.List;

import org.bukkit.OfflinePlayer;

import de.pro_crafting.region.PlayerQueryType;
import de.pro_crafting.region.Region;
import de.pro_crafting.region.provider.Provider;

public class Domain {
	private Region region;
	private PlayerQueryType type;
	private Provider provider;
	
	public Domain(Region region, PlayerQueryType type, Provider provider)
	{
		this.region = region;
		this.type = type;
		this.provider = provider;
	}
	
	public List<String> getPlayers()
	{
		return provider.getPlayers(region, type);
	}
	
	public void add(OfflinePlayer player)
	{
		provider.add(region, player, type);
	}
	
	public void remove(OfflinePlayer player)
	{
		provider.remove(region, player, type);
	}
	
	public void clear()
	{
		provider.clear(region, type);
	}
	
	public boolean contains(OfflinePlayer player)
	{
		List<String> players = this.getPlayers();
		String wanted = player.getName();
		for (String playerName : players)
		{
			if (playerName.equalsIgnoreCase(wanted))
			{
				return true;
			}
		}
		return false;
	}
}
