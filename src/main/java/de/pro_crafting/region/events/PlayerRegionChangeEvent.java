package de.pro_crafting.region.events;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import de.pro_crafting.region.Region;

public class PlayerRegionChangeEvent extends Event implements Cancellable {
	private Player player;
	private List<Region> from;
	private List<Region> to;
	private boolean entering;
	private static final HandlerList handlers = new HandlerList();
	private boolean isCancelled;

	public PlayerRegionChangeEvent(Player player, List<Region> from,
			List<Region> to, boolean entering) {
		this.player = player;
		this.from = from;
		this.to = to;
		this.entering = entering;
	}

	public Player getPlayer() {
		return this.player;
	}

	public List<Region> getFrom() {
		return this.from;
	}

	public List<Region> getTo() {
		return this.to;
	}

	public boolean isEntering() {
		return this.entering;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public boolean isCancelled() {
		return isCancelled;
	}

	public void setCancelled(boolean cancelled) {
		isCancelled = cancelled;
	}
}
