package de.pro_crafting.region.flags;

public enum Flag {
	TNT, Build, PVP, Use, Entry, Exit
}
