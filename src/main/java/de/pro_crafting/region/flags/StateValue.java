package de.pro_crafting.region.flags;

public enum StateValue {
	Deny, Allow, Default
}
