package de.pro_crafting.region.provider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;

import de.pro_crafting.common.Point;
import de.pro_crafting.region.ImmutableRegion;
import de.pro_crafting.region.PlayerQueryType;
import de.pro_crafting.region.Region;
import de.pro_crafting.region.flags.Flag;
import de.pro_crafting.region.flags.StateValue;

public abstract class Provider {
	private Map<String, Class<Region>> registeredRegionTypes;

	protected Provider() {
		this.registeredRegionTypes = new HashMap<String, Class<Region>>();
	}

	public Map<String, Class<Region>> getRegisteredRegionTypes() {
		return registeredRegionTypes;
	}

	public abstract boolean removeRegion(Region region);

	public abstract boolean createRegion(ImmutableRegion region);

	public abstract boolean save();

	public abstract Point getMinimumPoint(Region region);

	public abstract Point getMaximumPoint(Region region);

	public abstract List<String> getPlayers(Region region, PlayerQueryType type);

	public abstract void add(Region region, OfflinePlayer player, PlayerQueryType type);
	
	public abstract void remove(Region region, OfflinePlayer player, PlayerQueryType type);
	
	public abstract void clear(Region region, PlayerQueryType type);
	
	public abstract List<Region> getRegions(Location at);

	public abstract List<Region> getRegions(OfflinePlayer player, World where);

	public abstract Region getRegion(String id, World where);

	public abstract List<Region> getRegions(World where);

	public abstract void setFlag(Region region, Flag flag, StateValue to);
}
