package de.pro_crafting.region.provider.plugins;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.BukkitUtil;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import de.pro_crafting.common.Point;
import de.pro_crafting.region.ImmutableRegion;
import de.pro_crafting.region.PlayerQueryType;
import de.pro_crafting.region.Region;
import de.pro_crafting.region.flags.Flag;
import de.pro_crafting.region.flags.StateValue;
import de.pro_crafting.region.provider.Provider;

public class WorldGuard extends Provider {

	private RegionContainer container;
	private Map<Flag, StateFlag> stateFlags;
	private WorldGuardPlugin pl;
	
	public WorldGuard() {
		super();
		pl = (WorldGuardPlugin) Bukkit.getPluginManager().getPlugin(
				"WorldGuard");
		container = pl.getRegionContainer();
		stateFlags = new HashMap<Flag, StateFlag>();
		stateFlags.put(Flag.Build, DefaultFlag.BUILD);
		stateFlags.put(Flag.Entry, DefaultFlag.ENTRY);
		stateFlags.put(Flag.Exit, DefaultFlag.EXIT);
		stateFlags.put(Flag.PVP, DefaultFlag.PVP);
		stateFlags.put(Flag.TNT, DefaultFlag.TNT);
		stateFlags.put(Flag.Use, DefaultFlag.USE);
		
	}

	@Override
	public boolean removeRegion(Region region) {
		RegionManager rm = container.get(region.getWorld());
		if (!rm.hasRegion(region.getId())) {
			return false;
		}
		rm.removeRegion(region.getId());
		return true;
	}

	@Override
	public boolean createRegion(ImmutableRegion region) {
		RegionManager rm = this.container.get(region.getWorld());
		BlockVector v1 = new BlockVector(BukkitUtil.toVector(region.getMin()
				.toLocation(region.getWorld())));
		BlockVector v2 = new BlockVector(BukkitUtil.toVector(region.getMax()
				.toLocation(region.getWorld())));
		ProtectedCuboidRegion toAdd = new ProtectedCuboidRegion(region.getId(),
				v1, v2);
		rm.addRegion(toAdd);
		return true;
	}

	@Override
	public boolean save() {
		boolean successfull = true;
		for (World world : Bukkit.getWorlds()) {
			RegionManager rm = container.get(world);
			try {
				rm.save();
			} catch (Exception ex) {
				successfull = false;
			}
		}
		return successfull;
	}

	@Override
	public List<Region> getRegions(Location at) {
		RegionManager rm = container.get(at.getWorld());
		ApplicableRegionSet set = rm.getApplicableRegions(at);
		List<Region> ret = new ArrayList<Region>(set.size());
		for (ProtectedRegion rg : set)
		{
			ret.add(new Region(rg.getId(), at.getWorld(), this));
		}
		return ret;
	}

	@Override
	public Region getRegion(String id, World world) {
		RegionManager rm = container.get(world);
		ProtectedRegion rg = rm.getRegion(id.toLowerCase());
		if (rg == null)
			return null;
		return new Region(rg.getId(), world, this);
	}

	@Override
	public Point getMinimumPoint(Region region) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (rg == null)
			return null;
		return new Point(rg.getMinimumPoint().getBlockX(), rg.getMinimumPoint()
				.getBlockY(), rg.getMinimumPoint().getBlockZ());
	}

	@Override
	public Point getMaximumPoint(Region region) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (rg == null)
			return null;
		return new Point(rg.getMaximumPoint().getBlockX(), rg.getMaximumPoint()
				.getBlockY(), rg.getMaximumPoint().getBlockZ());
	}

	@Override
	public void setFlag(Region region, Flag flag, StateValue to) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (rg == null)
			return;
		if (to == StateValue.Allow) {
			rg.setFlag(this.stateFlags.get(flag), State.ALLOW);
		} else if (to == StateValue.Deny) {
			rg.setFlag(this.stateFlags.get(flag), State.DENY);
		} else {
			rg.setFlag(this.stateFlags.get(flag), null);
		}
	}

	@Override
	public List<Region> getRegions(World where) {
		RegionManager rm = container.get(where);
		List<Region> ret = new ArrayList<Region>();
		for (String id : rm.getRegions().keySet()) {
			ret.add(new Region(id, where, this));
		}
		return ret;
	}

	@Override
	public List<String> getPlayers(Region region, PlayerQueryType type) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		List<String> players = new ArrayList<String>();
		if (type == PlayerQueryType.Member) {
			players = new ArrayList<String>(rg.getMembers().getPlayers());
		} else {
			players = new ArrayList<String>(rg.getOwners().getPlayers());
		}
		return players;
	}

	@Override
	public void add(Region region, OfflinePlayer player, PlayerQueryType type) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (type == PlayerQueryType.Member) {
			rg.getMembers().addPlayer(player.getName());
		} else {
			rg.getOwners().addPlayer(player.getName());
		}
	}

	@Override
	public void remove(Region region, OfflinePlayer player, PlayerQueryType type) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (type == PlayerQueryType.Member) {
			rg.getMembers().removePlayer(player.getName());
		} else {
			rg.getOwners().removePlayer(player.getName());
		}
	}

	@Override
	public void clear(Region region, PlayerQueryType type) {
		RegionManager rm = container.get(region.getWorld());
		ProtectedRegion rg = rm.getRegion(region.getId());
		if (type == PlayerQueryType.Member) {
			rg.setMembers(new DefaultDomain());
		} else {
			rg.setOwners(new DefaultDomain());
		}
	}

	@Override
	public List<Region> getRegions(OfflinePlayer player, World where) {
		List<Region> regions = new ArrayList<Region>();
		RegionManager rm = container.get(where);
		LocalPlayer local = pl.wrapOfflinePlayer(player);
		for (ProtectedRegion region : rm.getRegions().values()) {
			if (region.isMember(local) || region.isOwner(local))
			{
				regions.add(new Region(region.getId(), where, this));
			}
		}
		return regions;
	}
}
